package Abstract;

public class Student extends Person {
    public String type;
    public int year;
    public int price;

    public Student(String type, int year, int price, String name, String address) {
        super(name, address);
        this.type = type;
        this.year = year;
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Student{" +
                "type='" + type + '\'' +
                ", year=" + year +
                ", price=" + price +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
