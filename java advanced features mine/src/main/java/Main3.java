import Abstract.Person;
import Abstract.Student;

import java.util.HashMap;
import java.util.Map;

public class Main3 {
    public static void main(String[] args) {
    Map<String, Person> persons = new HashMap<>();
    Person man1 = new Student("Licentiat", 4, 3450, "Vasile", "Cluj, Str. Titulescu");
    Person man2 = new Student("Masterand", 2, 3000, "Adrian", "Cluj, Str. Titulescu");
    persons.put("person1", man1);
    persons.put("person2", man2);

    for(Map.Entry<String, Person>person: persons.entrySet()){
        //System.out.println(student);
        String key = person.getKey();
        Person value = person.getValue();
        System.out.println(key + " " + value);
    }
  }
}