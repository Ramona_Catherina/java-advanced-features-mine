import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Set<String> colours = new HashSet<>();
        colours.add("Red");
        colours.add("Blue");
        colours.add("Green");
        colours.add("Yellow");
        colours.add("Purple");
        colours.remove("Green");

        for (String colour: colours) {
            System.out.println(colour);
    }
}

}
