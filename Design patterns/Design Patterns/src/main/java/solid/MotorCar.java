package solid;

public class MotorCar implements Car {
    private Engine engine;

    public MotorCar(){
        this.engine = new Engine();
    }

    public void start() {
        System.out.println("I am a motor car");
        engine.run();
    }

    public void accelerate(int kph) {
        System.out.println("I am so fast");
        engine.speed( kph);
    }
    public void stop() {
        System.out.println("Stop it already");
        engine.stop();
    }
}
