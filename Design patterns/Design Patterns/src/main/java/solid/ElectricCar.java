package solid;

public class ElectricCar implements Car {
    private Engine engine;

    public ElectricCar() {
        this.engine = new Engine();
    }

    public void start() {
        System.out.println("I am an electric car");
        engine.run();
    }

    public void accelerate(int kph) {
        System.out.println("I am a lot faster than a motor car");
        engine.speed( kph);
    }
    public void stop() {
        System.out.println("I should stop for charging");
        engine.stop();
    }
    public void regeneratingBrake(){
        engine.stop();
        System.out.println("Regenerating battery");
    }
}
