package solid;

public class Engine {
    public void run(){
        System.out.println("Engine is running at high speed");
    }
    public void stop(){
        System.out.println("Engine has stopped");
    }
    public void speed(int speed){
        System.out.println("Speed is" +  speed);
    }
}
