package designPatterns.Singleton;

public class Main {
    public static void main(String[] args) {
        Logging loggingService = Logging.getInstance();
        loggingService.printErrorMessage("Am creat pufosenia");
        Logging loggingService2 = Logging.getInstance();
        loggingService.printErrorMessage("Inca o pufosenie la colectie");
        System.out.println(loggingService);
    }
}
