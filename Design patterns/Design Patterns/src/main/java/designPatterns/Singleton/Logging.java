package designPatterns.Singleton;

public class Logging {
    private String lastErrorMessage;
    private static Logging instance = null;
    private Logging(){
        lastErrorMessage = new String();
    }
    public static Logging getInstance(){
        if (instance == null){
            instance = new Logging();
        } return instance;
    }

    public void printErrorMessage(String message){
        System.out.println(message);
        this.lastErrorMessage = message;
    }

    @Override
    public String toString() {
        return "Logging{" +
                "lastErrorMessage='" + lastErrorMessage + '\'' +
                '}';
    }
}
