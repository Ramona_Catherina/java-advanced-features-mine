package designPatterns.abstractFactory.pizza;

public class PizzaQuattroformaggi extends Pizza {
    private int size;

    public PizzaQuattroformaggi(int size) {
        this.size = size;
    }

    @Override
    public String getName() {
        return PizzaType.QUATTROFORMAGGI.toString();
    }

    @Override
    public String getIngredients() {
        return "Blat pufos, sos de rosii, mozzarella, gorgonzola, telemea, branza de burduf";
    }

    @Override
    public int getSize() {
        return size;
    }
}
