package designPatterns.abstractFactory.pizza;

public enum PizzaType {
    QUATTROSTAGIONI, MARGHERITA, QUATTROFORMAGGI
}
