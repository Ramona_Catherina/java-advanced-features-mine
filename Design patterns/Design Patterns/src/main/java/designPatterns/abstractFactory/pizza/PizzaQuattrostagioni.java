package designPatterns.abstractFactory.pizza;

public class PizzaQuattrostagioni extends Pizza{
    private int size;

    public PizzaQuattrostagioni(int size) {
        this.size = size;
    }

    @Override
    public String getName() {
        return PizzaType.QUATTROSTAGIONI.toString();
    }

    @Override
    public String getIngredients() {
        return "Blat pufos, sos de rosii, mozzarella, ciuperci, salam, ardei";
    }

    @Override
    public int getSize() {
        return size;
    }
}
