package designPatterns.abstractFactory.factories;

import designPatterns.abstractFactory.pizza.*;

public class PizzaFactory implements AbstractFactory {
    @Override
    public Pizza create(PizzaType type, int size) {
        switch(type){
            case MARGHERITA:
                return new PizzaMargherita(size);

            case QUATTROFORMAGGI:
                return new PizzaQuattroformaggi(size);

            case QUATTROSTAGIONI:
                return new PizzaQuattrostagioni(size);

            default: return null;

        }
    }
}
