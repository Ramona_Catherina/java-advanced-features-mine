package designPatterns.builder;

public class House {
    private String foundation;
    private String basement;
    private String walls;
    private String roof;
    private int roomNumber;

    public void setFoundation(String foundation) {
        this.foundation = foundation;
    }

    public void setBasement(String basement) {
        this.basement = basement;
    }

    public void setWalls(String walls) {
        this.walls = walls;
    }

    public void setRoof(String roof) {
        this.roof = roof;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    @Override
    public String toString() {
        return "House{" +
                "foundation='" + foundation + '\'' +
                ", basement='" + basement + '\'' +
                ", walls='" + walls + '\'' +
                ", roof='" + roof + '\'' +
                ", roomNumber=" + roomNumber +
                '}';
    }

    public static class HouseBuilder {
        private House house;
        private String walls;
        private String basement;
        private String roof;
        private String foundation;
        private int roomNumber;

        public HouseBuilder(String foundation, String walls, String roof) {

            this.foundation = foundation;
            this.walls = walls;
            this.roof = roof;
        }

    public HouseBuilder addBasement(String basement){
            this.basement = basement;
            return this;
    }

    public HouseBuilder addRooms(int roomNumber){
            this.roomNumber = roomNumber;
            return this;
    }

    public House build(){
        House house = new House();
        house.setFoundation(foundation);
        house.setBasement(basement);
        house.setRoof(roof);
        house.setWalls(walls);
        house.setRoomNumber(roomNumber);
        return house;
    }

  }


}

