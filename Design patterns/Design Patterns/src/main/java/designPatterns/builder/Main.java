package designPatterns.builder;

public class Main {
    public static void main(String[] args) {
        House.HouseBuilder mansionBuilder = new House.HouseBuilder("Beton", "Lemn", "Sarpanta");
        House basicHouse = mansionBuilder.build();
        House.HouseBuilder castleBuilder = new House.HouseBuilder("Beton", "Caramida", "Tigla");
        House complexHouse = castleBuilder.addBasement("Pivnita").addRooms(5).build();
        System.out.println(complexHouse);
        System.out.println(basicHouse);
    }
}
